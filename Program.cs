﻿
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
int choice = 0;
int guessNumber = 0;
int tryingCount = 0;
int greatScore = int.MaxValue;
int randomNumber;
string topPlayer = "";
do
{
    Console.WriteLine("------Menu Trò chơi------");
    Console.WriteLine("1. Bắt đầu trò chơi.");
    Console.WriteLine("2. Xem kỷ lục.");
    Console.WriteLine("3. Thoát trò chơi.");
    Console.WriteLine("Chọn một lựa chọn (1/2/3): ");

    choice = int.Parse(Console.ReadLine());
    randomNumber = Random.Shared.Next(0, 100);
    switch (choice)
    {
        case 1:
            while (guessNumber != randomNumber)
            {
                Console.WriteLine("\nSố của bạn dự đoán nằm trong khoảng từ 0 đến 99");
                Console.WriteLine("Nhâp số bạn muốn đoán: ");
                guessNumber = int.Parse(Console.ReadLine());
                tryingCount++;
                if (guessNumber == randomNumber)
                {
                    Console.WriteLine("Xin chúc mừng bạn đã đoán đúng!");
                }
                else if (guessNumber < randomNumber)
                {
                    Console.WriteLine("Số bạn đoán hơi thấp");
                }
                else
                {
                    Console.WriteLine("Số bạn đoán quá cao");
                }
            }

            if (tryingCount < greatScore)
            {
                Console.WriteLine($"Congrate! bạn đã lập kỷ lục mới với {tryingCount} lần đoán.");
                Console.WriteLine("\nNhập danh xưng của bạn: ");
                topPlayer = Console.ReadLine();
                greatScore = tryingCount;
            }
            break;
        case 2:
            if (greatScore == int.MaxValue)
            {
                Console.WriteLine("Kỷ lục chưa được phá, Chơi để phá kỷ lục nào");
            }
            else
            {
                Console.WriteLine($"Kỷ lục hiện tại cao nhất là '{greatScore}' bởi người chơi '{topPlayer}'");
            }
            break;
        case 3:
            Console.WriteLine("\nCảm ơn bạn đã tham gia trò chơi");
            break;
        default:
            Console.WriteLine("Nhập lựa chọn bảng trên");
            break;
    }

} while (choice != 3);